import argparse
from multiprocessing import Process, JoinableQueue, Queue, cpu_count
import re
import os
import signal
import sys
from time import time
#
import matplotlib.pyplot as pyplot


#
ARG_INPUT = 'i'
ARG_DIMENSIONS_WINDOW = 'dims'


#
SIZE_WINDOW_WIDTH = 12.0
SIZE_WINDOW_HEIGHT = 9.0


#
class CLArgs():
    def __init__(self):
        self.__parser = argparse.ArgumentParser()
        self.__parser.add_argument('-' + ARG_INPUT, required=False, nargs='+', type=lambda arg_file: self.__is_valid_file(arg_file))
        self.__parser.add_argument('--' + ARG_DIMENSIONS_WINDOW, required=False, nargs='+', type=float)
        self.__as_dictionary = None
    #
    def __is_valid_file(self, arg_file, required=False):
        if (not required):
            if (arg_file is None):
                return None
        if (not os.path.exists(arg_file)):
            self.__parser.error("File does not exist:\n\t{}".format(arg_file))
        else:
            return arg_file
    #
    def get_args(self):
        if (self.__as_dictionary is None):
            self.__as_dictionary = vars(self.__parser.parse_args())
        return self.__as_dictionary


#
class SignalHandler():
    def __init__(self):
        self.__register()
    #
    def __register_exit_signals(self):
        signal.signal(signal.SIGINT, SignalHandler._handle_exit_signals)
        signal.signal(signal.SIGTSTP, SignalHandler._handle_exit_signals)
    #
    def _handle_exit_signals(signum, frame):
        print("Exiting with {}".format(signum))
        exit(0)
    #
    def __register(self):
        self.__register_exit_signals()


#
class FileBundle():
    def __init__(self, list_paths):
        self.__list_paths = list_paths
        self.__list_paths_files = None
        #
        self.__setup()
    #
    def __is_valid_extension(self, path_file, extensions=['csv']):
        re_match = re.findall(r'(?<=\.)[^\.]+$', path_file)
        # print(re_match)
        if (len(re_match) < 1):
            return False
        #
        extension = re_match[0]
        return (extension in extensions)
    #
    def __iterate_path(self, path):
        if os.path.isfile(path):
            path_file = os.path.abspath(path)
            yield path_file
        #
        elif os.path.isdir(path):
            for root, dirs, files in os.walk(path):
                for file in files:
                    path_file = os.path.join(root, file)
                    yield path_file
    #
    def __get_valid_files(self):
        self.__list_paths_files = []
        #
        for path in self.__list_paths:
            # print(path)
            for path_file in self.__iterate_path(path):
                if (not self.__is_valid_extension(path_file)):
                    continue
                #
                # print("\t{}".format(path_file))
                self.__list_paths_files.append(path_file)
        #
        lambda_ = lambda p_f: os.path.basename(p_f)
        self.__list_paths_files = sorted(self.__list_paths_files, key=lambda_)
    #
    def __setup(self):
        self.__get_valid_files()
    #
    def get_traces(self):
        return self.__list_paths_files
    #
    def iterate_traces(self):
        for path_file in self.__list_paths_files:
            yield path_file


#
class CallTask():
    # Methods - Special
    def __init__(self, obj, *args, **kwargs):
        self.__obj = obj
        self.__args = args
        self.__kwargs = kwargs
        #
        self.__setup()

    def __call__(self):
        try:
            self.__obj(*self.__args, **self.__kwargs)
            return self.__obj
        except Exception as e:
            print("MethodTask Exception\n\t{}".format(e))
            return None
    # Methods - Private
    def __verify(self):
        if (not hasattr(self.__obj, '__call__')):
            raise Exception("Unable to run call on '{}'".format(self.__obj.__class__.__name__))
    #
    def __setup(self):
        self.__verify()


#
class Consumer(Process):
    # Methods - Instance
    def __init__(self, id_, queue_tasks, queue_results, timeout=0.5):
        self.__id = id_
        self.__queue_tasks = queue_tasks
        self.__queue_results = queue_results
        self.__timeout = max(0, timeout)
        #
        super().__init__()
    #
    def run(self):
        time_previous = time()
        while (True):
            try:
                task = self.__queue_tasks.get(timeout=self.__timeout)
                if (task is None):
                    self.__queue_tasks.task_done()
                    break
                #
                time_previous = time()
                result = task()
                self.__queue_tasks.task_done()
                # Append object to results
                self.__queue_results.put(result)
            #
            except Exception as e:
                diff_time = time() - time_previous
                if (diff_time >= self.__timeout):
                    break


#
class Trace():
    # Static functions
    def parse_line(line):
        re_match = re.findall(r'[\-0-9\.]+', line)
        if (len(re_match) < 1):
            return None
        list_values = [float(str_v) for str_v in re_match]
        return list_values
    # Instance functions
    def __init__(self, path_file):
        self.__path_file = path_file
        self.__list_samples = None
    #
    def __parse_file(self):
        self.__list_samples = []
        #
        with open(self.__path_file, 'r') as fd:
            for line in fd:
                line = line.strip()
                if (len(line) < 1):
                    continue
                #
                list_values = Trace.parse_line(line)
                if (list_values == None):
                    continue
                #
                self.__list_samples.append(list_values)
    #
    def __call__(self):
        self.__parse_file()
    #
    def get_path(self):
        return self.__path_file
    #
    def get_samples(self):
        return self.__list_samples


#
# def parse_line(line):
#     re_match = re.findall(r'[\-0-9\.]+', line)
#     if (len(re_match) < 1):
#         return None
#     list_values = [float(str_v) for str_v in re_match]
#     return list_values


# #
# def parse_file(path_file):
#     list_samples = []
#     #
#     with open(path_file, 'r') as fd:
#         for line in fd:
#             line = line.strip()
#             if (len(line) < 1):
#                 continue
#             #
#             list_values = parse_line(line)
#             if (list_values == None):
#                 continue
#             #
#             list_samples.append(list_values)
#     #
#     return list_samples


#
# def plot(file_bundle, window_width=12.0, window_height=9.0):
#     # Exit python3 if 'ctrl+c' provided in C.L.
#     signal_handler = SignalHandler()
#     #
#     pyplot.rcParams["figure.figsize"] = [window_width,  window_height]
#     #
#     # for path_file in file_bundle.iterate_traces():
#     list_paths_files = file_bundle.get_traces()
#     for path_file in list_paths_files:
#         list_samples = parse_file(path_file)
#         #
#         count_columns = len(list_samples[0])
#         count_samples = len(list_samples)
#         #
#         list_x = [x for x in range(0, count_samples)]
#         x_range = abs(min(list_x) - max(list_x))
#         x_adjustment = 0.01 * x_range
#         list_limit_x = [min(list_x) - x_adjustment, max(list_x) + x_adjustment]
#         #
#         figure = pyplot.figure(path_file, constrained_layout=True)
#         #
#         gridspec = figure.add_gridspec(nrows=count_columns, ncols=1, left=0.0875, right=0.975, bottom=0.0875, top=0.975)
#         #
#         for i_column in range(count_columns):
#             subplot = figure.add_subplot(gridspec[i_column:i_column+1, : ])
#             #
#             list_samples_column = [list_values[i_column] for list_values in list_samples]
#             #
#             #
#             y_range = abs(min(list_samples_column) - max(list_samples_column))
#             y_adjustment = 0.01 * y_range
#             list_limit_y = [min(list_samples_column) - y_adjustment, max(list_samples_column) + y_adjustment]
#             #
#             subplot.axis([*list_limit_x, *list_limit_y])
#             #
#             dict_grid_x = {'ls': '--'}
#             dict_grid_y = {'ls': '--'}
#             pyplot.grid(True, which='major', axis='x', **dict_grid_x)
#             pyplot.grid(True, which='major', axis='y', **dict_grid_y)
#             #
#             dict_kwargs_plot = {'lw': 0.5}
#             subplot.plot(list_x, list_samples_column, **dict_kwargs_plot)
#         #
#         print(path_file)
#         pyplot.show()


def plot_trace(trace, window_width=12.0, window_height=9.0):
    # Exit python3 if 'ctrl+c' provided in C.L.
    signal_handler = SignalHandler()
    #
    pyplot.rcParams["figure.figsize"] = [window_width,  window_height]
    #
    path_file = trace.get_path()
    list_samples = trace.get_samples()
    #
    count_columns = len(list_samples[0])
    count_samples = len(list_samples)
    #
    list_x = [x for x in range(0, count_samples)]
    x_range = abs(min(list_x) - max(list_x))
    x_adjustment = 0.01 * x_range
    list_limit_x = [min(list_x) - x_adjustment, max(list_x) + x_adjustment]
    #
    figure = pyplot.figure(path_file, constrained_layout=True)
    #
    gridspec = figure.add_gridspec(nrows=count_columns, ncols=1, left=0.0875, right=0.975, bottom=0.0875, top=0.975)
    #
    for i_column in range(count_columns):
        subplot = figure.add_subplot(gridspec[i_column:i_column+1, : ])
        #
        list_samples_column = [list_values[i_column] for list_values in list_samples]
        #
        #
        y_range = abs(min(list_samples_column) - max(list_samples_column))
        y_adjustment = 0.01 * y_range
        list_limit_y = [min(list_samples_column) - y_adjustment, max(list_samples_column) + y_adjustment]
        #
        subplot.axis([*list_limit_x, *list_limit_y])
        #
        dict_grid_x = {'ls': '--'}
        dict_grid_y = {'ls': '--'}
        pyplot.grid(True, which='major', axis='x', **dict_grid_x)
        pyplot.grid(True, which='major', axis='y', **dict_grid_y)
        #
        dict_kwargs_plot = {'lw': 0.5}
        subplot.plot(list_x, list_samples_column, **dict_kwargs_plot)
    #
    print(path_file)
    pyplot.show()


#
#
def create_new_consumer(id_, queue_tasks, queue_results, timeout=1.5):
    id_ = "{}__{}".format(id_, time())
    consumer = Consumer(id_, queue_tasks, queue_results, timeout=timeout)
    #
    consumer.start()
    #
    return consumer


#
def multiprocessing_plot(file_bundle, window_width=SIZE_WINDOW_WIDTH, window_height=SIZE_WINDOW_HEIGHT, timeout=1.5, count_queue_files=2):
    queue_tasks = JoinableQueue()
    queue_results = Queue()
    #
    i_file = -1
    i_consumer = 0
    #
    list_paths_files = file_bundle.get_traces()
    count_files = len(list_paths_files)
    count_files_remaining = count_files
    count_queue_files_pending = 0
    #
    #
    consumer = create_new_consumer(i_consumer, queue_tasks, queue_results, timeout=timeout)
    i_consumer += 1
    #
    while (count_files_remaining > 0):
        #
        for _ in range(count_queue_files):
            if (count_queue_files_pending > count_queue_files):
                break
            #
            if (i_file < (count_files - 1)):
                i_file += 1
                count_queue_files_pending += 1
                #
                path_file = list_paths_files[i_file]
                trace = Trace(path_file)
                #
                args = []
                kwargs = {}
                task = CallTask(trace, *args, **kwargs)
                #
                queue_tasks.put(task)
        #
        if (not consumer.is_alive()):
            consumer = create_new_consumer(i_consumer, queue_tasks, queue_results, timeout=timeout)
            i_consumer += 1
        #
        try:
            trace = queue_results.get_nowait()
            if (trace is not None):
                count_files_remaining -= 1
                count_queue_files_pending -= 1
            #
            plot_trace(trace, window_width=window_width, window_height=window_height)
        #
        except Exception:
            pass
    #
    queue_tasks.join()
#
#


#
def get_window_dimensions(list_dimensions):
    list_dimensions_ = [SIZE_WINDOW_WIDTH, SIZE_WINDOW_HEIGHT]
    if (list_dimensions != None):
        for i in range(0, 2):
            if (len(list_dimensions) > i):
                list_dimensions_[i] = list_dimensions[i]
    #
    return list_dimensions_


#
def main():
    args = CLArgs().get_args()
    #
    file_bundle = FileBundle(args[ARG_INPUT])
    #
    list_dimensions = get_window_dimensions(args[ARG_DIMENSIONS_WINDOW])
    # plot(file_bundle, window_width=list_dimensions[0], window_height=list_dimensions[1])
    #
    multiprocessing_plot(file_bundle, window_width=list_dimensions[0], window_height=list_dimensions[1])


#
if (__name__ == '__main__'):
    main()
